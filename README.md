## Problem solving on [Hackerrank](https://www.hackerrank.com/)
### Contains  solutions for multiple tasks in algoritms and data structure domains

**Almost all solutions are implemented with Java 8**  
    
*Functional programming style of Scala does not suit well for classical imperative algorithms*
  
    
Gradle is used as a build tool
 

[My profile page](https://www.hackerrank.com/marat_sef)