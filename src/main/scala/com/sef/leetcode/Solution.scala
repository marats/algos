package com.sef.leetcode

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Solution {


  def longSubstrLength(s: String): Int = {
    val n = s.length
    if (n < 2) return n

    val set = mutable.LinkedHashSet[Char]()
    val map = mutable.Map[Int, String]()

    var i = 0
    while (i < n) {
      //if a char is not in the set then put it there
      val c = s.charAt(i)
      if (!set.contains(c)) {
        set += c
      } else {
        //we found a double
        //store everything we still have and go on
        if (set.nonEmpty) {
          val subString = set.mkString
          map.put(subString.length, subString)
          // find last non-repeating character
          val tail = subString.reverse.takeWhile(el => el != c)
          //finish step
          set.clear()
          if (!tail.isEmpty) {
            set ++= tail.reverse.toCharArray
          }
          set += c
        }
      }
      i += 1
    }
    //secure the case when the whole string consists of only unique letters
    if (set.nonEmpty && !map.contains(set.mkString.length)) {
      val subString = set.mkString
      map.put(subString.length, subString)
    }
    map.keySet.max
  }

  def numDecodings(s: String): Int = {
    if (s == null || s.isEmpty || s.startsWith("0")) return 0
    if (s.length == 1) return s.length

    val dp = ArrayBuffer.fill(s.length)(0)

    dp(0) = if (s.charAt(0).asDigit != 0) {
      1
    } else {
      0
    }

    var i = 1
    while (i < s.length) {
      //check one digit case
      if (s.charAt(i).asDigit != 0) dp(i) = dp(i - 1)
      //check two digit case
      if (s.charAt(i - 1).asDigit != 0 && ((s.charAt(i - 1).asDigit == 2 && s.charAt(i).asDigit <= 6) || (s.charAt(i - 1).asDigit == 1))) {
        dp(i) += (if (i < 2) {
          1
        } else dp(i - 2))
      }
      i += 1
    }
    dp.last
  }

  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val diffMap = mutable.TreeMap[Int, Int]()
    val indices = new ArrayBuffer[Int]()
    var i = 0
    for (el <- nums) {
      diffMap.put(target - el, i)
      i += 1
    }

    i = 0
    var found = false
    while (i < nums.length && !found) {
      val res = diffMap.get(nums(i))
      res match {
        case Some(e) =>
          if (i != e) {
            indices += (i, e)
            found = true
          }
        case _ =>
      }
      i += 1
    }

    indices.toArray
  }

  // Complete the extraLongFactorials function below.
  def extraLongFactorials(n: Int) {
    //fill with zeros
    val fact = ArrayBuffer.fill[BigInt](n + 1) (0)
    fact(0) = 1
    var i = 1
    while (i <= n) {
      fact(i) = i * fact(i - 1)
      i += 1
    }
    println(fact(n))
  }

  // find common child for two strings
  def commonChild(s1: String, s2: String): Int = {
    val n = s1.length
    val counters = Array.ofDim[Int](n + 1, n + 1)
    var i = 0
    var j = 0

    while (i <= n) {
      while (j <= n) {
        if (i == 0 || j == 0) counters(i)(j) = 0
        else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
          counters(i)(j) = counters(i - 1)(j - 1) + 1
        } else {
          counters(i)(j) = Math.max(counters(i - 1)(j), counters(i)(j - 1))
        }
        j += 1
      }
      i += 1
    }

    counters(n)(n)
  }


  def main(args: Array[String]) {
//    numDecodings("17")
//    extraLongFactorials(25)
//    twoSum(Array(3, 2, 4), 6)
//    commonChild("ABCDEF", "FBDAMN")

    commonChild("HARRY",
      "SALLY")
  }

}
