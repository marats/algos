package com.sef.hackerrank.util;

import java.util.Comparator;

/**
 * Comparator that enables secondary sort by name
 * where the first sort is invoked based on the player's score
 */
public class Checker implements Comparator<Player> {
    // complete this method
    public int compare(Player a, Player b) {
        int res;
        if (a.score != b.score) {
            res = Integer.compare(a.score, b.score);
        } else {
            final String aName = a.name;
            final String bName = b.name;

            res = aName.compareTo(bName);
        }
        return res;
    }
}
