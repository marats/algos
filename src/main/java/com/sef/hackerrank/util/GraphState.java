package com.sef.hackerrank.util;

public enum GraphState {
    Unvisited, Visited, Visiting
}
