package com.sef.hackerrank.util;

/**
 * Parse result wrapper
 */
public class ParseResult implements Cloneable {
    private int invalid;
    private String parsed;

    public ParseResult(int inv, String p) {
        invalid = inv;
        parsed = p;
    }


    public static ParseResult min(ParseResult r1, ParseResult r2) {
        if (r1 == null) {
            return r2;
        } else if (r2 == null) {
            return r1;
        }

        return r2.invalid < r1.invalid ? r2 : r1;
    }

    public int getInvalid() {
        return invalid;
    }

    public void setInvalid(int invalid) {
        this.invalid = invalid;
    }

    public String getParsed() {
        return parsed;
    }

    public void setParsed(String parsed) {
        this.parsed = parsed;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ParseResult result = (ParseResult) super.clone();
        result.setInvalid(this.invalid);
        result.setParsed(this.parsed);
        return result;
    }
}
