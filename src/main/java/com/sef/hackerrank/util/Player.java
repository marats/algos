package com.sef.hackerrank.util;

/**
 * Player entity
 */
public class Player {
    String name;
    int score;

    Player(String name, int score) {
        this.name = name;
        this.score = score;
    }
}
