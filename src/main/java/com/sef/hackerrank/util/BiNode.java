package com.sef.hackerrank.util;

/**
 * Created on 15.09.19.
 */
public class BiNode {
    public BiNode node1;
    public BiNode node2;
    public int data;
    public BiNode(int d) {
        data = d;
    }
}
