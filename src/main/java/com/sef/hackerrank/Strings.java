package com.sef.hackerrank;

import com.sef.hackerrank.util.LocationPair;

import java.util.*;


/**
 * String manipulation
 */
public class Strings {

    //Multi Search: Given a string band an array of smaller strings T, design a method to search b for each small string in T.
    public static Map<String, List<Integer>> searchAll(String big, String[] smalls) {
        final Map<String, List<Integer>> res = new HashMap<>();
        for (int i = 0; i < smalls.length; i++) {
            if (big.contains(smalls[i])) {
                List<Integer> locations = new ArrayList<>();
                int index = 0;
                int pos = 0;
                while (index != -1 && pos < big.length()) {

                    index = big.substring(pos).indexOf(smalls[i]);
                    if (index != -1) {
                        pos += index;
                        locations.add(pos);
                        pos += smalls[i].length();
                    }
                }
                res.put(smalls[i], locations);
            }
        }
        return res;
    }


    public static String clean(String str) {
        char[] punctuation = {',', '"', '!', '.', '\'', '?', ','};
        for (char c : punctuation) {
            str = str.replace(c, ' ');
        }
        return str.replace(" ", "").toLowerCase();
    }


    // Complete the funnyString function below.
    static String funnyString(String s) {
        int[] chars = new int[s.length()];
        int[] reverseChars = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = s.charAt(i);
            reverseChars[s.length() - 1 - i] = chars[i];
        }
        //get sums
        int d1;
        int d2;
        for (int i = 0; i < chars.length; i++) {
            if (i + 1 < chars.length) {
                d1 = Math.abs(chars[i + 1] - chars[i]);
                d2 = Math.abs(reverseChars[i + 1] - reverseChars[i]);
                if (d1 != d2) {
                    return ("Not Funny");
                }
            }
        }

        return ("Funny");
    }


    //word distance
    public static LocationPair findClosest(String[] words, String word1, String word2) {
        int location1 = 0;
        int location2 = words.length;
        for (int i = 0; i < words.length; i++) {

            if (words[i].equals(word1) && (location2 - i) < (location2 - location1)) {
                location1 = i;
            } else if (words[i].equals(word2) && (i - location1) < (location2 - location1)) {
                location2 = i;
            }

        }
        return new LocationPair(location1, location2);
    }


    static int sherlockAndAnagrams(String s) {
        int count = 0;
        Set<String> parts = new LinkedHashSet<>(Arrays.asList(s.split("")));

        for (int k = 0; k < s.length() - 1; k++) {
            //take a substring and check its occurrences against all remaining ones
            for (int l = k + 1; l <= s.length(); l++) {
                if (s.substring(k, l).equals(s)) {
                    continue;
                }
                parts.add(s.substring(k, l));
            }

        }
        Iterator<String> iterator = parts.iterator();
        while (iterator.hasNext()) {
            final String substr = iterator.next();
            final int len = substr.length();
            for (int i = 0; i < s.length(); i++) {
                if (i + len <= s.length()) {
                    String substr1 = s.substring(i, i + len);
                    if (!substr.equals(substr1)) {
                        continue;
                    }
                    for (int j = i + 1; j < s.length(); j++) {
                        if (j + len <= s.length()) {
                            final String substr2 = s.substring(j, j + len);
                            if (hasAnagram(substr1, substr2)) {
                                count++;
                            }
                        }
                    }
                }
            }
        }

        return count;
    }


    static boolean hasAnagram(String substr1, String substr2) {
        final int len = substr1.length();
        char[] a = new char[26];
        char[] b = new char[26];

        int j = 0;
        while (j < len) {
            a[substr1.charAt(j) - 'a']++;
            b[substr2.charAt(j) - 'a']++;
            j++;
        }
        for (j = 0; j < 26; j++) {
            if (a[j] != b[j]) {
                return false;
            }
        }
        return true;
    }

    static int alternatingCharacters(String s) {
        int n = s.length();

        int deletions = 0;
        for (int i = 1; i < n; i++) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                deletions++;
             }

        }
        return deletions;
    }

    /** -- testing --- */
    public static void main(String[] args) {
        String big = "mississippi";
        String[] smalls = {"is", "ppi", "hi", "sis", "i", "mississippi"};
        Map<String, List<Integer>> locations = searchAll(big, smalls);
        System.out.println(locations.toString());

        funnyString("acxz");
        funnyString("uvzxrumuztyqyvpnji");

        int c = sherlockAndAnagrams("ifailuhkqq");
    }

}
