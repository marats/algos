package com.sef.hackerrank;

import com.sef.hackerrank.util.TreeNode;

import java.util.*;

/**
 * Trees subdomain
 */
public class Trees {

    private static int last = 0;

    public TreeNode insertIntoBST(TreeNode root, int val) {
        final TreeNode node = new TreeNode(val);
        if (root == null) {
            return node;
        }
        if (root.data > val) {
            if (root.left == null) {
                root.left = node;
            } else {
                insertIntoBST(root.left, val);
            }
        } else if (root.data < val) {
            if (root.right == null) {
                root.right = node;
            } else {
                insertIntoBST(root.right, val);
            }
        }
        return root;
    }


    static boolean checkBST(TreeNode root, boolean isLeft) {
        //check left
        if (root == null) {
            return true;
        }
        //check left subtree
        checkBST(root.left, true);
        if (last != 0) {
            if (isLeft) {
                if (root.data < last) {
                    return false;
                }
            } else {  // right
                if (root.data <= last) {
                    return false;
                }
            }
        }

        last = root.data;
        checkBST(root.right, false);
        return true;
    }

    public static boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        int diff = Math.abs(getHeight(root.left) - getHeight(root.right));
        if (diff > 1) {
            return false;
        }
        return isBalanced(root.left) && isBalanced(root.right);

    }

    public static int getHeight(TreeNode root) {
        if (root == null) {
            return -1;// Base case
        }
        return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
    }


    public static TreeNode inorderSucc(TreeNode n) {
        if (n.right != null) {
            return leftMostChild(n);
        } else {
            TreeNode q = n;
            TreeNode x = q.parent;
            // Go up until we're on the left instead of right
            while (x != null && x.left != q) {
                q = x;
                x = x.parent;
            }
            return x;
        }
    }

    public static TreeNode leftMostChild(TreeNode n) {
        if (n == null) {
            return null;
        }
        while (n.left != null) {
            n = n.left;
        }
        return n;
    }

    public static void levelOrder(TreeNode root) {
        LinkedList<TreeNode> queue = new LinkedList<>();
        System.out.print(root.data);
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.removeFirst();
            if (node.left != null) {
                System.out.print(" " + node.left.data);
                queue.add(node.left);
            }
            if (node.right != null) {
                System.out.print(" " + node.right.data);
                queue.add(node.right);
            }
        }
    }

    //lowest common ancestor
    public static TreeNode lca(TreeNode root, int v1, int v2) {
        // Write your code here.
        Map<Integer, TreeNode> node1 = findNode(root, v1, new TreeMap<>());
        Map<Integer, TreeNode> node2 = findNode(root, v2, new TreeMap<>());

        // trace up until common ancestor is found
        for (Map.Entry<Integer, TreeNode> entry : node1.entrySet()) {
            if (node2.containsKey(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public static Map<Integer, TreeNode> findNode(TreeNode root, int v, Map<Integer, TreeNode> parents) {
        if (root == null) {
            return null;
        }
        if (root.data == v) {
            return parents;
        }

        if (v > root.data) {
            findNode(root.right, v, parents);
        } else {
            findNode(root.left, v, parents);
        }
        return parents;
    }



    public static TreeNode insert(TreeNode root, int data) {
        if (root == null) {
            return new TreeNode(data);
        } else {
            TreeNode cur;
            if (data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    static void decode(String s, TreeNode root) {
        StringBuilder builder = new StringBuilder();
        TreeNode n = root;
        for (int i = 0; i < s.length(); i++) {
            char edge = s.charAt(i);
            if (edge == '1') {
                n = n.right;
            } else if (edge == '0') {
                n = n.left;
            }
            builder.append(n.data);
        }
        System.out.println(builder);
    }



    //testing
    public static void main(String[] args) {

        TreeNode node1 = new TreeNode(1);
        node1.right = new TreeNode(2);
        node1.right.right = new TreeNode(5);
        node1.right.right.right = new TreeNode(6);
        node1.right.right.left = new TreeNode(3);
        node1.right.right.left.right = new TreeNode(4);
        levelOrder(node1);
    }
}





