package com.sef.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tasks for greedy algorithms
 */
public class Greedy {

    public static int maxMinutes2(int[] massages) {
        int oneAway = 0;
        int twoAway = 0;
        for (int i = massages.length - 1; i >= 0; i--) {
            int bestWith = massages[i] + twoAway;
            int bestWithout = oneAway;
            int current = Math.max(bestWith, bestWithout);
            twoAway = oneAway;
            oneAway = current;
        }
        return oneAway;
    }

    public static int calculateSum(int[] appointments, int index) {
        int res = 0;
        for (int i = index; i < appointments.length; i += 2) {
            res += appointments[i];
        }
        return res;
    }

    static int minimumAbsoluteDifference(int[] arr) {
        Arrays.sort(arr);
        int minDiff = Math.abs(arr[0] - arr[1]);

        for (int i = 1; i < arr.length - 1; i++) {
            int j = i + 1;
            final int diff = Math.abs(arr[i] - arr[j]);
            if (minDiff == 0) {
                break;
            } else if (diff < minDiff) {
                minDiff = diff;
            }
        }

        return minDiff;
    }


    // Complete the maxMin function below.
    static int maxMin(int k, int[] arr) {
        Arrays.sort(arr);
        int n = arr.length;
        int fairness = arr[k - 1] - arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (i + k == arr.length) {
                break;
            }
            int kFairness = arr[i + k - 1] - arr[i];
            if (kFairness < fairness) {
                fairness = kFairness;
            }
        }
        return fairness;
    }

    public static void main(String[] args) {
        int[] massages = {30, 15, 60, 75, 45, 15, 15, 45};
        System.out.println(maxMinutes2(massages));
    }
}
