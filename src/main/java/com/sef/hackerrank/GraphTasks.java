package com.sef.hackerrank;

import com.sef.hackerrank.util.Graph;
import com.sef.hackerrank.util.GraphState;
import com.sef.hackerrank.util.Node;

import java.util.*;

/**
 * Tasks for graphs
 */
public class GraphTasks {

    public static HashSet<String> setupDictionary(String[] words) {
        final HashSet<String> set = new HashSet<>();
        Collections.addAll(set, words);
        return set;
    }

    public static LinkedList<String> transform(String start, String stop, String[] words) {
        HashSet<String> dict = setupDictionary(words);
        HashSet<String> visited = new HashSet<>();
        return transform(visited, start, stop, dict, null);
    }


    // word path
    public static LinkedList<String> transform(HashSet<String> visited, String startWord, String stopWord, Set<String> dictionary, LinkedList<String> path) {
        if (path == null) {
            path = new LinkedList<>();
        }

        visited.add(startWord);

        if (visited.contains(stopWord) || !dictionary.contains(stopWord)) {
            return path;
        }

        int counter = path.size();
        final List<String> possibleWords = getPossibleWords(startWord);
        for (String word : possibleWords) {
            if (dictionary.contains(word) && !visited.contains(word)) {
                path.add(word);
            }
        }

        if (counter == path.size()) {
            return null;
        }

        return transform(visited, path.getLast(), stopWord, dictionary, path);
    }

    public static List<String> getPossibleWords(final String word) {
        final List<String> res = new LinkedList<>();
        for (int i = 0; i < word.length(); i++) {
            for (char j = 'a'; j <= 'z'; j++) {
                String newWord = word.substring(0, i) + j + word.substring(i + 1);
                res.add(newWord);
            }
        }

        return res;
    }

    //Route_Between_Nodes
    public static boolean search(Graph g, Node start, Node end) {
        if (start == end) {
            return true;
        }
        LinkedList<Node> queue = new LinkedList<>();
        for (Node node : g.getNodes()) {
            node.state = GraphState.Unvisited;
        }

        queue.add(start);
        while (!queue.isEmpty()) {
            final Node node = queue.removeFirst();

            for (Node n : node.getAdjacent()) {
                if (n == end) {
                    return true;
                } else {
                    n.state = GraphState.Visited;
                    queue.add(n);
                }
            }
            node.state = GraphState.Visited;
        }
        return false;
    }


    /**
     * Even forest
     *
     * @param nodes     number of nodes
     * @param edges     number of edges
     * @param fromNodes start edge nodes
     * @param toNodes   end nodes
     * @return number of trees
     */
    static int evenForest(int nodes, int edges, List<Integer> fromNodes, List<Integer> toNodes) {
        Map<Integer, List<Integer>> revertedMap = new TreeMap<>();

        int counter = 0;

        final Iterator<Integer> iteratorFrom = fromNodes.iterator();
        final Iterator<Integer> iteratorTo = toNodes.iterator();
        while (iteratorFrom.hasNext() && iteratorTo.hasNext()) {
            final Integer from = iteratorFrom.next();
            final Integer to = iteratorTo.next();
            revertedMap.putIfAbsent(to, new ArrayList<>());
            revertedMap.get(to).add(from);
        }

        for (Map.Entry<Integer, List<Integer>> entry : revertedMap.entrySet()) {
            final Integer root = entry.getKey();
            int childNum = dfs(root, new HashSet<>(), revertedMap);
            if (root != 1 && (childNum + 1) % 2 == 0) {
                counter++;
            }
        }

        return counter;
    }

    public static int dfs(int root, Set<Integer> visited, Map<Integer, List<Integer>> childMap) {
        if (root == 0 || !childMap.containsKey(root)) {
            return 0;
        }
        //retrive all children nodes
        final List<Integer> children = childMap.get(root);
        for (Integer n : children) {
            if (!visited.contains(n)) {
                visited.add(n);
                dfs(n, visited, childMap);
            }
        }
        return visited.size();
    }


    // Complete the bfs function below.
    static int[] bfs(int n, int m, int[][] edges, int s) {

        Map<Integer, Set<Integer>> adjacentMap = new HashMap<>();
        for (int i = 0; i < edges.length; i++) {

            adjacentMap.putIfAbsent(edges[i][0], new TreeSet<>());
            final Set<Integer> adjacents = adjacentMap.get(edges[i][0]);
            adjacents.add(edges[i][1]);
            //reverse connection
            adjacentMap.putIfAbsent(edges[i][1], new TreeSet<>());
            final Set<Integer> adjacentsR = adjacentMap.get(edges[i][1]);
            adjacentsR.add(edges[i][0]);
        }

        List<Integer> pathes = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i != s) {
                pathes.add(bfsHelper(s, i, m, adjacentMap));
            }
        }
        System.out.println();
        return pathes.stream().mapToInt(i -> i).toArray();
    }

    static int bfsHelper(int s, int dest, int edges, Map<Integer, Set<Integer>> adjacentMap) {
        int path = 0;
        int count = 1;
        final LinkedList<Integer> queue = new LinkedList<>();
        queue.add(s);

        Set<Integer> visited = new HashSet<>();

        while (!queue.isEmpty() && count <= edges) {
            final Integer node = queue.removeFirst();
            path += 6;
            if (!adjacentMap.containsKey(node)) {
                continue;
            }
            for (Integer n : adjacentMap.get(node)) {
                if (n == dest) {
                    return path;
                } else {
                    if (!visited.contains(node)) {
                        queue.add(n);
                        visited.add(n);
                    }
                }
            }
            count++;

            visited.add(node);
        }

        return -1;
    }


    /**
     * Finding the shortest path in a graph
     * Not finished yet!
     */
    static int findShortest(int graphNodes, int[] graphFrom, int[] graphTo, long[] ids, int val) {
        LinkedList<Integer> queue = new LinkedList<>();
        Map<Integer, List<Integer>> adjacent = new HashMap<>();
        //get adjacent mapping
        for (int i = 0; i < graphTo.length; i++) {
            int key = Math.min(graphFrom[i], graphTo[i]);
            int value = Math.max(graphFrom[i], graphTo[i]);
            List<Integer> adjNodes = adjacent.get(key);
            if (adjNodes == null) {
                adjNodes = new ArrayList<>();
            }
            adjNodes.add(value);
            adjacent.put(key, adjNodes);
        }

        // get all nodes of required color
        List<Integer> requiredNodes = new ArrayList<>();
        for (int i = 0; i < ids.length; i++) {
            if (ids[i] == val) {
                requiredNodes.add(i + 1);
            }
        }

        Map<Integer, Integer> res = new TreeMap<>();
        for (Integer node : requiredNodes) {
            // res.putAll(getPath(node, adjacent, ids));
        }

        return 1;
    }

    public static Map<Integer, Integer> getPath(int v, int i, long[] ids, int length) {
        if (i >= ids.length) {
            return null;
        }
        if (v == ids[i]) {
            Map<Integer, Integer> map = new HashMap<>();
            map.put(v, ++length);
        }
        getPath(v, ++i, ids, ++length);
        return null;
    }


    public static Graph createNewGraph() {
        Graph g = new Graph();
        Node[] temp = new Node[6];

        temp[0] = new Node("a", 3);
        temp[1] = new Node("b", 0);
        temp[2] = new Node("c", 0);
        temp[3] = new Node("d", 1);
        temp[4] = new Node("e", 1);
        temp[5] = new Node("f", 0);

        temp[0].addAdjacent(temp[1]);
        temp[0].addAdjacent(temp[2]);
        temp[0].addAdjacent(temp[3]);
        temp[3].addAdjacent(temp[4]);
        temp[4].addAdjacent(temp[5]);
        for (int i = 0; i < 6; i++) {
            g.addNode(temp[i]);
        }
        return g;
    }


    public static String clean(String str) {
        char[] punctuation = {',', '"', '!', '.', '\'', '?', ','};
        for (char c : punctuation) {
            str = str.replace(c, ' ');
        }
        return str.replace(" ", "").toLowerCase();
    }


    /**
     * -------testing--------------
     */
    public static void main(String [] a) {

//        List<Integer> from = new ArrayList<>(Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30));
//        List<Integer> to = new ArrayList<>(Arrays.asList(1, 2, 3, 2, 4, 4, 1, 5, 4, 4, 8, 2, 2, 8, 10, 1, 17, 18, 4, 15, 20, 2, 12, 21, 17, 5, 27, 4, 25));
//
//        evenForest(10, 9, from, to);

//        //int[][] array = {{3, 1}, {10, 1}, {10, 1}, {3, 1}, {1, 8}, {5, 2}};
//        bfs(5, 3, array, 1);
    }

}