package com.sef.hackerrank;

import java.util.*;

/**
 * Search algorithms
 */
public class Searching {

    static long triplets(int[] a, int[] b, int[] c) {
        long counter = 0;
        a = Arrays.stream(a).distinct().sorted().toArray();

        b = Arrays.stream(b).distinct().sorted().toArray();

        c = Arrays.stream(c).distinct().sorted().toArray();

        for (int value : b) {
            int i = 0;
            while (i < a.length && a[i] <= value) {
                i++;
            }

            int k = 0;
            while (k < c.length && c[k] <= value) {
                k++;
            }

            counter += i * k;
        }
        return counter;
    }

    /**
     * Finds number of all pairs which difference is equal to target
     *
     * @param k target difference
     * @return number of found pairs
     */
    static int pairs(int k, int[] arr) {
        Arrays.sort(arr);
        int n = arr.length;
        int counter = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                final int target = arr[j] - arr[i];
                if (target == k) {
                    counter++;
                } else if (target > k) {
                    break;
                }
            }
        }
        return counter;
    }


    static long minTime(long[] machines, long goal) {
        Arrays.sort(machines);
        int n = machines.length;
        final long[] initialCapacity = Arrays.copyOf(machines, n);

        int days = 0;
        //count how many machines required when most powerful selected first
        while (goal > 0) {
            for (int i = 0; i < n; i++) {
                if (goal <= 0) {
                    machines[i] -= 1;
                }
                if (machines[i] == 0) {
                    goal--;
                    machines[i] = initialCapacity[i];
                }
            }
            days += 1;
        }

        return days;
    }


    /**
     * Finds the maximum number of reserved rooms on one of the given days
     *
     * @param reservations list of all reservations in the last <code>n</code> days
     * @return max
     */
    public static int findMaxRooms(final List<Reservation> reservations) {
        final TreeMap<Integer, Integer> map = new TreeMap<>(); //key is a day, counter is value
        final List<Integer> checkIns = new ArrayList<>();
        final List<Integer> checkOuts = new ArrayList<>();

        for (final Reservation reservation : reservations) {
            checkIns.add(reservation.checkIn);
            checkOuts.add(reservation.checkOut);
        }

        Collections.sort(checkIns);
        Collections.sort(checkOuts);

        int maxDay = Math.max(checkIns.get(reservations.size() - 1), checkOuts.get(reservations.size() - 1));

        int counter = 0;
        for (int i = 1; i <= maxDay; i++) {
            final int currentDay = i;
            counter += checkIns.stream().parallel().filter(el -> el == currentDay).count();
            counter -= checkOuts.stream().filter(el -> el == currentDay).count();
            map.put(currentDay, counter);
        }
        if (map.isEmpty()) {
            return 0;
        }
        return map.lastEntry().getValue();
    }

    //reservation info
    // check-in and check-out dates are presented as numbers
    private static class Reservation {
        public Integer checkIn;
        public Integer checkOut;

        public Reservation(int in, int out) {
            this.checkIn = in;
            this.checkOut = out;
        }
    }


    public static void main(String[] args) {
        int[] a = {1, 3, 5};
        int[] b = {2, 3};
        int[] c = {1, 2, 3};
        long result = triplets(a, b, c);


        List<Reservation> list = new LinkedList<>();

        list.add(new Reservation(1, 3));
        list.add(new Reservation(2, 3));
        list.add(new Reservation(3, 5));
        list.add(new Reservation(2, 4));
        list.add(new Reservation(6, 7));
        list.add(new Reservation(5, 7));
        list.add(new Reservation(1, 4));
        list.add(new Reservation(5, 10));

        findMaxRooms(list);
    }

}
