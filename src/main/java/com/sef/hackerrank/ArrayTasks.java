package com.sef.hackerrank;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Tasks for arrays domain
 */
public class ArrayTasks {

    /**
     * Rotates(shifts to the left) the array by <code>d</code> elements
     *
     * @param a array to rotate
     * @param d number of rotations
     * @return rotated array
     */
    public static int[] rotLeft(int[] a, int d) {
        int n = a.length;

        for (int k = 0; k < d; k++) {
            int initValue = a[0];
            for (int i = 0; i < n - 1; i++) {
                a[i] = a[i + 1];
            }
            a[n - 1] = initValue;
        }
        return a;
    }

    public static char[] findLongestSubarray(char[] array) {
        final int n = array.length;
        char[] subArray = new char[0];
        for (int i = 0; i < n - 1; i++) {
            if (isEqual(array, i, n - 1)) {
                System.arraycopy(array, i, subArray, 0, n - i);
            }
        }
        return subArray;
    }

    public static boolean isEqual(char[] array, int start, int end) {
        int counter = 0;
        for (int i = start; i < end; i++) {
            if (Character.isLetter(array[i])) {
                counter++;
            } else if (Character.isDigit(array[i])) {
                counter--;
            }
        }
        return counter == 0;
    }


    //find majority element
    public static List<Integer> majorityElement(int[] nums) {
        Set<Integer> res = new HashSet<>();

        if (nums.length == 1 || nums.length == 2) {
            return java.util.Arrays.stream(nums).distinct().boxed().collect(Collectors.toList());
        }

        Arrays.sort(nums);
        final int n = nums.length;
        final int majorFactor = n / 3;

        int counter = 0;

        for (int i = 0; i < n; i++) {
            int current = nums[i];
            int j = i;

            while (j < n && j <= majorFactor + i) {
                if (nums[j] == current) {
                    counter++;
                }
                j++;
            }

            if (counter > majorFactor) {
                res.add(current);
            }
            counter = 0;
        }
        return res.stream().collect(Collectors.toList());
    }


    public static int findCommonSmallest(int[] a, int[] b) {
        HashSet<Integer> set = new HashSet<>();
        for (int value : a) {
            set.add(value);
        }

        int min = Integer.MAX_VALUE;
        for (int i = 0; i < a.length; i++) {
            if (set.contains(b[i])) {
                if (b[i] < min) {
                    min = b[i];
                }
            }
        }
        return min;
    }


    public static Integer[] findMultiCommon(int[] a, int[] b, int[] c) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < a.length; i++) {
            map.put(a[i], 1);
        }
        checkArray(b, map);
        checkArray(c, map);
        return map.entrySet().stream().filter(entry -> entry.getValue() == 2).map(Map.Entry::getKey).toArray(Integer[]::new);
    }

    // helper method
    private static void checkArray(int[] arr, Map<Integer, Integer> map) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            if (set.contains(arr[i])) {
                continue;
            }
            set.add(arr[i]);

            if (map.containsKey(arr[i])) {
                int val = map.get(arr[i]);
                map.put(arr[i], ++val);
            } else {
                map.put(arr[i], 1);
            }
        }
    }

    static int pairs(int k, int[] arr) {
        java.util.Arrays.sort(arr);
        final int n = arr.length;
        int counter = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                final int target = arr[j] - arr[i];
                if (target == k) {
                    counter++;
                } else if (target > k) {
                    break;
                }
            }
        }
        return counter;
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        java.util.Arrays.sort(nums);//prepare the array
        Set<List<Integer>> res = new HashSet<>();

        HashMap<Integer, Integer> diffMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            diffMap.put(-nums[i], i);
        }

        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            for (int j = 1; j < nums.length; j++) {
                if (j > 1 && nums[j] == nums[j - 1]) {
                    continue;
                }

                final int sum2 = nums[i] + nums[j];
                if (diffMap.containsKey(sum2) && diffMap.get(sum2) != i && diffMap.get(sum2) != j && i != j) {
                    List<Integer> list = new ArrayList<>();
                    list.add(nums[i]);
                    list.add(nums[j]);
                    list.add(nums[diffMap.get(sum2)]);
                    Collections.sort(list);
                    res.add(list);
                }
            }
        }
        return res.stream().collect(Collectors.toList());
    }


    //codility
    public static int solution(int[] a) {
        // write your code in Java SE 8
        Set<Integer> peaks = new TreeSet<>();
        //determine peaks
        for (int i = 1; i < a.length - 1; i++) {
            if (a[i] > a[i - 1] && a[i + 1] < a[i]) {
                peaks.add(i);
            }
        }

        if (peaks.size() <= 1) {
            return peaks.size();
        }

        // find maximum flags
        int k = peaks.size();

        int max = 1;
        //start from the second element
        while (k >= 2) {

            final Iterator<Integer> pIterator = peaks.iterator();
            int prev = pIterator.next();
            int j = 1;
            while (pIterator.hasNext() && j <= k) {
                int current = pIterator.next();
                if (current - prev >= k) {
                    j++;
                }
                prev = current;

            }

            if (j > max) {
                max = j;
            }

            k--;
        }
        return max;
    }


    public static int minimumSwaps(int[] arr) {
        int counter = 0;
        for (int i = 0; i < arr.length; ) {
            if (arr[i] != i + 1) {
                int tmp = arr[arr[i] - 1];
                arr[arr[i] - 1] = arr[i];
                arr[i] = tmp;

                counter++;
            } else {
                i++;
            }
        }
        return counter;
    }




    public static void main(String[] args) {
//        int arr[] = {3, 2, 3};
//        majorityElement(arr);
//
//       int[] arr = {-1, 0, 1, 2, -1, -4};
//       int[] array = {1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2};
        int[] array = {0, 0, 0, 0, 0, 1, 0, 1, 0, 1};
        solution(array);

    }

}
