package com.sef.hackerrank;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

/**
 *  Bit manipulation tasks
 */
public class BitDigits {
    //calculates number of twos in the input array
    public static int countTwos(int[] arr) {
        final int n = arr.length;
        int counter = 0;
        for (int i = 0; i < n; i++) {
            Integer[] digits = extractDigits(arr[i]);
            for (int j = 0; j < digits.length; j++) {
                if (digits[j] == 2) {
                    counter++;
                }
            }

        }
        return counter;
    }

    /**
     * Extracts separate digits from a number(123->[1,2,3])
     * @param num number to extract digits from
     * @return arrays with separate digits
     */
    public static Integer[] extractDigits(int num) {
        ArrayList<Integer> digits = new ArrayList<>();
        while (num != 0) {
            int digit = num % 10;
            num = num / 10;
            digits.add(digit);
        }
        return digits.stream().toArray(Integer[]::new);
    }


    // swap digits
    public static int swapDigits(int k, int num) {
        int length = (int) (Math.log10(num) + 1);
        final LinkedList<Integer> stack = new LinkedList<>();

        while (num > 0) {
            stack.push(num % 10);
            num /= 10;
        }

        final Integer[] a = new Integer[length];
        stack.toArray(a);
        Arrays.sort(a);

        int res = 0;
        for (int i = length - 1; i > 0; i--) {
            res +=  a[i] * Math.pow(10, i);
        }
        return res;
    }



    // Complete the counterGame function below.
    static String counterGame(long n) {
        if (n == 0 || n == 1) {
            return "Louise";
        }
        String winner = "Louise";
        while (n != 1) {
            final long res = n & (n - 1);
            if (res == 0) {
                n = n >> 1;
            } else {
                n -= res;
            }

            if (n == 1) {
                break;
            }
            winner = winner.equals("Louise") ? "Richard" : "Louise";
        }
        return winner;

    }

    public static long flippingBits(long n) {
        for (int i = 0; i < 32;  i++) {
            if (((1 << i) & n) != 0) {
                //clear the bit
                n = ~(1 << i) & n;
            } else {
                n = (1 << i) | n;
            }
        }
        final String s = Long.toBinaryString(n);
        return n & 0x00000000ffffffffL;
    }


    /**
     * Sum of two numbers without use of "+" operation
     * @param a first operand
     * @param b second operand
     * @return  result of summation
     */
    public static long sum(long a, long b) {
        int n = 64;
        int carry = 0;
        long result = 0;
        for (int i = 0; i < n; i++) {
            int ai = ((1 << i) & a) != 0 ? 1 : 0;
            int bi = ((1 << i) & b) != 0 ? 1 : 0;
            if (ai + bi == 1) {
                if (carry == 0) {
                    result = (1 << i) | result;
                } else {
                    result = ~(1 << i) & result;
                    carry = 1;
                }
            } else if (ai + bi == 0) {
                if (carry == 1) {
                    result = (1 << i) | result;
                    carry = 0;
                }
            } else if ((ai + bi) > 1) {
                if (carry == 0) {
                    result = ~(1 << i) & result;
                    carry = 1;
                } else {
                    result = (1 << i) | result;
                    carry = 1;
                }

            }
        }
        return result;
    }


    public static void main(String[] args) {
        //swapDigits(2, 3580);

        Random rand = new Random();

        int n = rand.nextInt(300000) + 1;
        int missing = rand.nextInt(n + 1);
        ArrayList<String> array = initialize(n, missing);
        System.out.println("The array contains all numbers but one from 0 to " + n + ", except for " + missing);


        String s = "1001011";
        String result1 = counterGame(132);
        flippingBits(2147483647);

        Integer[] digits = extractDigits(5678);
        Arrays.stream(digits).forEach(System.out::println);
    }

    public static ArrayList<String> initialize(int n, int missing) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (i == 7) {
                continue;
            }
            list.add(Integer.toBinaryString(i));

        }
        return list;
    }
}
