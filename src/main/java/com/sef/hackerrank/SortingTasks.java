package com.sef.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Sorting tasks
 */
public class SortingTasks {

    // Complete the activityNotifications function below.
    static int activityNotifications(int[] expenditure, int d) {
        int counter = 0;
        ArrayList<Integer> arr = new ArrayList<>();//helper array
        for (int i = 0; i < expenditure.length; i++) {
            if (i < d) {
                arr.add(expenditure[i]);
                continue;
            }
            //Collections.sort(arr);
            double median = getMedian(arr.toArray(new Integer[d]));
            if (expenditure[i] >= median * 2) {
                counter++;
            }
            arr.remove(0);
            arr.add(expenditure[i]);

        }
        return counter;
    }


    private static double getMedian(final Integer[] arr) {
        //sorting
        int n = arr.length;
        if (n % 2 == 0) {
            return (arr[n / 2] + arr[n / 2 - 1]) / 2.0;
        }
        return arr[n / 2];
    }


    // Complete the maximumToys function below.
    static int maximumToys(int[] prices, int k) {
        Arrays.sort(prices);
        int rest = k;
        int i = 0;
        while (rest - prices[i] >= 0) {
            rest -= prices[i];
            i++;
        }
        return i;
    }


    public static void main(String[] args) {
        int [] expenditure = {2, 3, 4, 2, 3, 6, 8, 4, 5};
        int[] exps = {1, 2, 3, 4, 4};
        int result = activityNotifications(expenditure, 5);

//        ArrayList<Integer> list = new ArrayList<>();
//        int i = 5;
//        list.add(i++);
//        int a[] = new int[1];
//        a[0]++;
//        System.out.println();
    }
}
