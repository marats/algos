package com.sef.hackerrank;

import java.util.*;

/**
 * Tasks for heap as data structure
 */
public class HeapTasks {

    static int cookies(int k, int[] arr) {
        if (arr.length < 2) {
            return 0;
        }
        final PriorityQueue<Integer> heap = new PriorityQueue<>();
        for (int value : arr) {
            heap.offer(value);
        }

        int counter = 0;
        int i = 0;
        while (heap.size() > 1 && heap.peek() < k) {
            Integer firstLeast = heap.poll();
            Integer secondLeast = heap.poll();
            Integer newLeast = firstLeast + 2 * secondLeast;
            heap.offer(newLeast);
            counter++;

        }
        if (heap.peek() < k) {
            return -1;
        }

        return counter;
    }


    public static List<String> findItinerary(List<List<String>> tickets) {
        final String start = "JFK";
        final LinkedList<String> res = new LinkedList<>();
        Map<String, PriorityQueue<String>> flights = new HashMap<>();

        for (List<String> ticket : tickets) {
            final String from = ticket.get(0);
            flights.putIfAbsent(from, new PriorityQueue<>());
            flights.get(from).offer(ticket.get(1));
        }
        res.add(start);

        final PriorityQueue<String> queue = flights.get(start);
        res.add(queue.poll());


        for (int i = 0; i < tickets.size() - 1; i++) {
            final String last = res.getLast();
            if (flights.containsKey(last)) {
                res.add(flights.get(last).poll());
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 1};
        cookies(10, arr);
    }
}
